const createRole = require('./createRole');
const an = require('./create');
const check = require('./check');


const acl = (rolesObject) => ({
  createRole: createRole(rolesObject),
  an: an(rolesObject),
  a: an(rolesObject),
  check: check(rolesObject),
});

module.exports = acl;
