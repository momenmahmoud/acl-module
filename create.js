const { pathToRegexp, match } = require("path-to-regexp");

const setWhen = (targetPath) => (whenFunction) => { targetPath.when = whenFunction };

const setPath = (targetMethod) => (path) => {
  const regex = pathToRegexp(path);
  const matchPath = match(path);
  targetMethod[path] = { regex };
  const hasParams = Object.keys(matchPath(path).params).length > 0;
  if (hasParams) targetMethod[path].match = matchPath;
  return { when: setWhen(targetMethod[path]) };
};

const setMethod = (targetRole) => (method) => {
  if (!targetRole[method]) targetRole[method] = {};
  return {
    from: setPath(targetRole[method]),
    to: setPath(targetRole[method]),
  };
};

const an = (roles) => (roleName) => ({ can: setMethod(roles[roleName]) });

module.exports = an;
